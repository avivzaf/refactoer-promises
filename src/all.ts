import { IterableInput } from "./types.js";

export async function all(promises: IterableInput): Promise<any[]> {
    const results = [];

    promises = await promises;

    for (const p of promises) {
        results.push(await p);
    }
    return results;
}
