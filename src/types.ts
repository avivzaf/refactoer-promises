export type IterableInput = Iterable<any> | Promise<Iterable<any>>;

export type ArrayInput = any[];

export interface IPropsInput {
    [key: string]: Promise<any>;
}

export interface IPropsOutput {
    [key: string]: any;
}
