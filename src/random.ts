export const random = (max: number, min = 0): number =>
    min + Math.round(Math.random() * (max - min));
