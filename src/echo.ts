import log from "@ajar/marker";
import { delay } from "./delay.js";

export const echo = async (msg: string, ms: number): Promise<string> => {
    log.yellow(`--> start ${msg}`);
    await delay(ms);
    log.blue(`finish <-- ${msg}`);
    return msg;
};
